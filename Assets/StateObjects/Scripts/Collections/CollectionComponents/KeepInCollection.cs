﻿namespace StateObjects.Collections.CollectionComponents
{
    using UnityEngine;

    public abstract class KeepInCollection<TCollection, TItemType> : MonoBehaviour where TCollection : ScriptableCollection<TItemType>
    {
        [SerializeField]
        private TCollection _collection;
        [SerializeField]
        private TItemType _item;

        private void OnEnable()
        {
            _collection.Add(_item);
        }

        private void OnDisable()
        {
            _collection.Remove(_item);
        }
    }
}
