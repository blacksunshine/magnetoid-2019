﻿namespace StateObjects.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class ScriptableCollection<T> : ScriptableCollection, IList<T>
    {
        [SerializeField]
        private List<T> _collection = new List<T>();

        [SerializeField]
        private List<T> _currentCollection = new List<T>();

        public event Action<T> OnAdded;
        public event Action<T> OnRemoved;
        
        private void OnEnable()
        {
#if UNITY_EDITOR
            if(UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            {
#endif
                _currentCollection = _collection.Select(arg1 => arg1).ToList();
#if UNITY_EDITOR
            }
#endif
        }

        private void OnDisable()
        {
            _currentCollection.Clear();
        }

        public void Add(T item)
        {
            _currentCollection.Add(item);
            OnChanged?.Invoke();
            OnAdded?.Invoke(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _currentCollection.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            if(!_currentCollection.Remove(item))
            {
                return false;
            }

            OnChanged?.Invoke();
            OnRemoved?.Invoke(item);

            return true;
        }

        public int RemoveRange(IEnumerable<T> range)
        {
            var count = 0;
            foreach(var item in range)
            {
                if(_currentCollection.Remove(item))
                    count++;
            }

            return count;
        }

        public IEnumerable<T> GetEnumerable()
        {
            return _currentCollection;
        }

        public void Clear()
        {
            if(OnRemoved == null)
            {
                _currentCollection.Clear();
                return;
            }

            var copy = new List<T>(_currentCollection);
            _currentCollection.Clear();

            foreach(var item in copy)
            {
                OnRemoved?.Invoke(item);
            }
        }

        public bool Contains(T item)
        {
            return _currentCollection.Contains(item);
        }

        public override int Count => Application.isPlaying? _currentCollection.Count:_collection.Count;

        public bool IsReadOnly => false;

        public IEnumerator<T> GetEnumerator()
        {
            return _currentCollection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return _currentCollection.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _currentCollection.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _currentCollection.RemoveAt(index);
        }

        public T this[int index]
        {
            get { return _currentCollection[index]; }
            set { _currentCollection[index] = value; }
        }
    }
}
