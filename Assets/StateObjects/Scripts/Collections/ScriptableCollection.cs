﻿namespace StateObjects.Collections
{
    using System;
    using UnityEngine;

    public abstract class ScriptableCollection : ScriptableObject
    {
        public abstract int Count { get; }
        public Action OnChanged;
    }
}
