﻿namespace StateObjects.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using JetBrains.Annotations;

    public class ScriptableDictionary<TKey, TValue> : ScriptableDictionary, IDictionary<TKey, TValue>
    {
        private readonly IDictionary<TKey, TValue> _currentDictionary = new Dictionary<TKey, TValue>();

        public event Action<TKey, TValue> OnAdded;
        public new event Action<TKey, TValue> OnChanged;
        public event Action<TKey> OnRemoved;
        
        public void Add(TKey key, TValue value)
        {
            _currentDictionary.Add(key, value);
            if (OnAdded != null)
                OnAdded(key, value);
        }

        public bool ContainsKey(TKey key)
        {
            return _currentDictionary.ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            if(!_currentDictionary.Remove(key))
            {
                return false;
            }

            if (OnRemoved != null)
                OnRemoved(key);


            return true;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _currentDictionary.TryGetValue(key, out value);
        }

        public TValue this[TKey key]
        {
            get { return _currentDictionary[key]; }
            set
            {
                _currentDictionary[key] = value;
                if(OnChanged != null) OnChanged(key, value);

            }
        }

        public ICollection<TKey> Keys => _currentDictionary.Keys;

        public ICollection<TValue> Values => _currentDictionary.Values;

        public TValue GetValue(TKey key)
        {
            TValue value;
            _currentDictionary.TryGetValue(key, out value);

            return value;
        }

        public IDictionary<TKey, TValue> GetEnumerable()
        {
            return _currentDictionary;
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _currentDictionary.Add(item);
            if (OnAdded != null)
                OnAdded(item.Key, item.Value);

        }

        public void Clear()
        {
            if (OnRemoved == null)
            {
                _currentDictionary.Clear();
                return;
            }

            var keys = new List<TKey>(_currentDictionary.Keys);
            _currentDictionary.Clear();

            foreach (var key in keys)
            {
                // ReSharper disable once PossibleNullReferenceException
                OnRemoved(key);
            }

        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _currentDictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _currentDictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if(!_currentDictionary.Remove(item))
            {
                return false;
            }

            if (OnRemoved != null)
                OnRemoved(item.Key);

            return true;
        }

        public override int Count => _currentDictionary.Count;

        public bool IsReadOnly => false;

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _currentDictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
