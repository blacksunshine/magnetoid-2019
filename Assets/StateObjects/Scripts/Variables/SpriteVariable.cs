﻿namespace StateObjects.Variables
{
    using UnityEngine;

    public class SpriteVariable : ScriptableVariable<Sprite>
    {
    }
}
