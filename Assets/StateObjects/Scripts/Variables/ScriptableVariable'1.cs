﻿namespace StateObjects.Variables
{
    using System;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using UnityEngine;

    public interface IScriptableVariableListener<T>
    {
        void OnPropertyChanged(ScriptableVariable<T> variable);
    }

    public abstract class ScriptableVariable<T> : ScriptableObject
    {
        [SerializeField]
        private T _defaultValue;
        [SerializeField]
        private T _currentValue;
        
        public T Value
        {
            get { return _currentValue; }
            set
            {
                _currentValue = value;
                CallListener();
            }
        }

        private readonly List<IScriptableVariableListener<T>> _listeners = new List<IScriptableVariableListener<T>>();

        public void AddListener([NotNull] IScriptableVariableListener<T> listener)
        {
            if(listener == null)
            {
                throw new ArgumentNullException("listener");
            }
            _listeners.Add(listener);
        }

        public void RemoveListener([NotNull] IScriptableVariableListener<T> listener)
        {
            if(listener == null)
            {
                throw new ArgumentNullException("listener");
            }
            _listeners.Remove(listener);
        }

        private void CallListener()
        {
            for(var i = _listeners.Count - 1; i >= 0; --i)
            {
                _listeners[i].OnPropertyChanged(this);
            }
        }

        private void Awake()
        {
            hideFlags = hideFlags & HideFlags.DontUnloadUnusedAsset;
        }

        private void OnEnable()
        {
            Value = _defaultValue;
        }

        private void OnValidate()
        {
            if(Application.isPlaying)
            {
                CallListener();
            }
        }
    }
}
