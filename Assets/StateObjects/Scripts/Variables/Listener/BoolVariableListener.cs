﻿namespace StateObjects.Variables.Listener
{
    using UnityEngine;
    using UnityEngine.Events;

    public class BoolVariableListener : MonoBehaviour, IScriptableVariableListener<bool>
    {
        [SerializeField] private BoolVariable _boolVariable;
        [SerializeField] private bool _triggerOnStart;
        [SerializeField] private UnityEvent _onEnable;
        [SerializeField] private UnityEvent _onDisable;

        private void Awake()
        {
            _boolVariable.AddListener(this);
        }

        private void Start()
        {
            if (_triggerOnStart)
            {
                OnPropertyChanged(_boolVariable);
            }
        }

        public void OnPropertyChanged(ScriptableVariable<bool> variable)
        {
            if (variable.Value)
            {
                _onEnable.Invoke();
            }
            else
            {
                _onDisable.Invoke();
            }
        }
    }
}