﻿namespace StateObjects.Variables
{
    using UnityEngine;

    public class Vector2IntVariable : ScriptableVariable<Vector2Int>
    {
    }
}
