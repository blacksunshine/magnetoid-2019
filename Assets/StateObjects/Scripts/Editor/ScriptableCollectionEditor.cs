﻿using StateObjects.Collections;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace StateObjects.Editor
{
    //Deactivated because currently it's useless
    //[CustomEditor(typeof(ScriptableCollection), true)]
    public class ScriptableCollectionEditor : UnityEditor.Editor
    {
        private ReorderableList _list;

        private void OnEnable()
        {
            _list = new ReorderableList(
                serializedObject,
                EditorApplication.isPlaying ? serializedObject.FindProperty("_currentCollection") : serializedObject.FindProperty("_collection"),
                true,
                true,
                true,
                true
            )
            {
                drawElementCallback = DrawElementCallback,
                elementHeightCallback = ElementHeightCallback,
                drawHeaderCallback = DrawHeaderCallback,
                draggable = false,
            };

        }

        private static void DrawHeaderCallback(Rect rect)
        {
            EditorGUI.LabelField(rect, EditorApplication.isPlaying ? "Current Collection":"Collection");
        }

        private float ElementHeightCallback(int index)
        {
            var property = _list.serializedProperty.GetArrayElementAtIndex(index);

            var height = EditorGUI.GetPropertyHeight(property);

            if (property.propertyType == SerializedPropertyType.Generic)
            {
                return height + 5;
            }

            return height;
        }

        private void DrawElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            var property = _list.serializedProperty.GetArrayElementAtIndex(index);

            var oldIndent = EditorGUI.indentLevel;
            if (property.propertyType == SerializedPropertyType.Generic)
            {
                EditorGUI.indentLevel++;
                EditorGUI.PropertyField(rect, property, true);
            }
            else
            {
                EditorGUI.PropertyField(rect, property, GUIContent.none, true);
            }
            EditorGUI.indentLevel = oldIndent;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.LabelField(new GUIContent("Count"), new GUIContent(((ScriptableCollection)target).Count.ToString()), GUIStyle.none);
            _list.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
