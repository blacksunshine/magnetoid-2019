﻿using StateObjects.Collections;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Editor
{
    [CustomEditor(typeof(ScriptableDictionary), true)]
    public class ScriptableDictionaryEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField(new GUIContent("Count"), new GUIContent(((ScriptableCollection)target).Count.ToString()));
            DrawDefaultInspector();
        }
    }
}
