﻿using StateObjects.Variables;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Editor
{
    public static class CreateScriptableVariables
    {
        /// <summary>
        /// Create new asset from <see cref="ScriptableObject"/> type with unique name at
        /// selected folder in project window. Asset creation can be cancelled by pressing
        /// escape key when asset is initially being named.
        /// </summary>
        /// <typeparam name="T">Type of scriptable object.</typeparam>
        private static void CreateAsset<T>() where T : ScriptableObject
        {
            var asset = ScriptableObject.CreateInstance<T>();
            ProjectWindowUtil.CreateAsset(asset, "New " + typeof(T).Name + ".asset");
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/Float")]
        private static void CreateFloatVariable()
        {
            CreateAsset<FloatVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/Integer")]
        private static void CreateIntVariable()
        {
            CreateAsset<IntVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/String")]
        private static void CreateStringVariable()
        {
            CreateAsset<StringVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/Bool")]
        private static void CreateBoolVariable()
        {
            CreateAsset<BoolVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/Sprite")]
        private static void CreateSpriteVariable()
        {
            CreateAsset<SpriteVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/Vector2Int")]
        private static void CreateVector2IntVariable()
        {
            CreateAsset<Vector2IntVariable>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableVariables/DateTime")]
        private static void CreateDateTimeVariable()
        {
            CreateAsset<DateTimeVariable>();
        }
    }
}
