﻿using StateObjects.Variables;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Editor
{
    [CustomEditor(typeof(ScriptableVariable<>), true)]
    public class ScriptableVariableEditor : UnityEditor.Editor {

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            if (!EditorApplication.isPlaying)
            {
                var property = serializedObject.FindProperty("_defaultValue");
                if (property == null)
                {
                    EditorGUILayout.LabelField("Variable not displayable in editor.");
                    return;
                }
                EditorGUILayout.PropertyField(property, new GUIContent("Default Value"));
            }
            else
            {
                var valueProperty = serializedObject.FindProperty("_currentValue");
                if (valueProperty == null)
                {
                    EditorGUILayout.LabelField("Variable not displayable in editor.");
                    return;
                }
                EditorGUILayout.PropertyField(valueProperty, new GUIContent("Current Value"));
            }
            if(EditorGUI.EndChangeCheck())
            {
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
