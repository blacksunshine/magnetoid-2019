﻿using StateObjects.References;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Editor
{
    [CustomPropertyDrawer(typeof(ScriptableVariableReference), true)]
    public class ScriptableVariableReferenceDrawer : PropertyDrawer
    {
        /// <summary>
        /// Options to display in the popup to select constant or variable.
        /// </summary>
        private readonly string[] _popupOptions =
            { "Use Constant", "Use Variable" };

        /// <summary> Cached style to use to draw the popup button. </summary>
        private GUIStyle _popupStyle;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (_popupStyle == null)
            {
                _popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions")) { imagePosition = ImagePosition.ImageOnly };
            }

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, label);

            EditorGUI.BeginChangeCheck();

            // Get properties
            var useConstant = property.FindPropertyRelative("_useConstant");
            var constantValue = property.FindPropertyRelative("_constantValue");
            var variable = property.FindPropertyRelative("_variable");

            // Calculate rect for configuration button
            var buttonRect = new Rect(position);
            buttonRect.yMin += _popupStyle.margin.top;
            buttonRect.width = _popupStyle.fixedWidth + _popupStyle.margin.right;
            position.xMin = buttonRect.xMax;

            // Store old indent level and set it to 0, the PrefixLabel takes care of it
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, _popupOptions, _popupStyle);

            useConstant.boolValue = result == 0;

            EditorGUI.PropertyField(position,
                useConstant.boolValue ? constantValue : variable,
                GUIContent.none);

            if(EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();

                if(EditorApplication.isPlaying)
                {
                    var target = property.serializedObject.targetObject;

                    var targetRight = (ScriptableVariableReference)fieldInfo.GetValue(target);

                    targetRight.OnValidate();
                    /*
                    var listenerProperty = fieldInfo.FieldType.GetProperty("OnPropertyChangedListener");

                    if(listenerProperty == null)
                    {
                        Debug.LogError("This should never happen");
                        return;
                    }
                    listenerProperty.GetSetMethod()
                                    .Invoke(targetRight, new[] {listenerProperty.GetGetMethod().Invoke(targetRight, null)});

                    var callListener = targetRight.GetType().GetMethod("EditorFunction");
                    if (callListener == null)
                    {
                        Debug.LogError("This should never happen");
                        return;
                    }
                    callListener.Invoke(targetRight, null);*/
                }
            }

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }
    }
}
