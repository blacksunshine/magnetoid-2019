﻿namespace StateObjects.References
{
    using System;
    using UnityEngine;
    using Variables;

    public interface IScriptableVariableReferenceListener<T, T1> where T : ScriptableVariable<T1>
    {
        void OnPropertyChanged(ScriptableVariableReference<T, T1> variable);
    }

    [Serializable]
    public class ScriptableVariableReference<T, T1> : ScriptableVariableReference, IScriptableVariableListener<T1> where T : ScriptableVariable<T1>
    {
        [SerializeField]
#pragma warning disable 649
        private bool _useConstant;
        [SerializeField]
        private T1 _constantValue;
        [SerializeField]
        private T _variable;
#pragma warning restore 649

        private IScriptableVariableReferenceListener<T, T1> _listener;

        public IScriptableVariableReferenceListener<T, T1> OnPropertyChangedListener
        {
            get { return _listener; }
            set
            {
                if(_listener != null && _variable != null)
                {
                    _variable.RemoveListener(this);
                }
                _listener = value;
                if (_listener != null && _variable != null)
                {
                    _variable.AddListener(this);
                }
            }
        }

        public ScriptableVariableReference()
        {
            
        }

        public ScriptableVariableReference(T1 value)
        {
            _constantValue = value;
        }

        public T1 Value
        {
            get { return _useConstant ? _constantValue : _variable == null ? default(T1) : _variable.Value; }
            set
            {
                if (_useConstant)
                    _constantValue = value;
                else
                    _variable.Value = value;
            }
        }

        public void OnPropertyChanged(ScriptableVariable<T1> variable)
        {
            _listener.OnPropertyChanged(this);
        }
#if UNITY_EDITOR
        public override void OnValidate()
        {
            if(_listener == null) return;
            _listener.OnPropertyChanged(this);
        }
#endif
    }
}
