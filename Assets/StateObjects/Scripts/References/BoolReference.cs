﻿namespace StateObjects.References
{
    using System;
    using Variables;

    [Serializable]
    public class BoolReference : ScriptableVariableReference<BoolVariable, bool>
    {
        public static implicit operator bool(BoolReference reference)
        {
            return reference.Value;
        }
    }
}
