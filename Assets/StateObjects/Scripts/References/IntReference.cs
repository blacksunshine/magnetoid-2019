﻿namespace StateObjects.References
{
    using System;
    using Variables;

    [Serializable]
    public class IntReference : ScriptableVariableReference<IntVariable, int>
    {
        public static implicit operator int(IntReference reference)
        {
            return reference.Value;
        }
    }
}
