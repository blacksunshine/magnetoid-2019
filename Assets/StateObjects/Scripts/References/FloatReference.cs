﻿namespace StateObjects.References
{
    using System;
    using Variables;

    [Serializable]
    public class FloatReference : ScriptableVariableReference<FloatVariable, float>
    {
        public static implicit operator float(FloatReference reference)
        {
            return reference.Value;
        }
    }
}
