﻿namespace StateObjects.References
{
    using System;
    using Variables;

    [Serializable]
    public class DateTimeReference : ScriptableVariableReference<DateTimeVariable, DateTime>
    {
        public static implicit operator DateTime(DateTimeReference reference)
        {
            return reference.Value;
        }
    }
}
