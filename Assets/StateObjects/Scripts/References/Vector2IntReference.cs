﻿namespace StateObjects.References
{
    using System;
    using UnityEngine;
    using Variables;

    [Serializable]
    public class Vector2IntReference : ScriptableVariableReference<Vector2IntVariable, Vector2Int>
    {
        public override string ToString()
        {
            return Value.ToString();
        }

        public static implicit operator Vector2Int(Vector2IntReference reference)
        {
            return reference.Value;
        }
    }
}
