﻿namespace StateObjects.References
{
    using System;
    using Variables;

    [Serializable]
    public class StringReference : ScriptableVariableReference<StringVariable, string>
    {
        public override string ToString()
        {
            return Value;
        }

        public static implicit operator string(StringReference reference)
        {
            return reference.Value;
        }
    }
}
