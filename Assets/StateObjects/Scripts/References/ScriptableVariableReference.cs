﻿namespace StateObjects.References
{
    using System;

    [Serializable]
    public abstract class ScriptableVariableReference
    {
#if UNITY_EDITOR
        public abstract void OnValidate();
#endif
    }
}
