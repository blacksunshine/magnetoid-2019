﻿namespace StateObjects.References
{
    using System;
    using UnityEngine;
    using Variables;

    [Serializable]
    public class SpriteReference : ScriptableVariableReference<SpriteVariable, Sprite>
    {

    }
}
