﻿namespace StateObjects.Events.ListenerComponents
{
    using System;
    using Events;
    using UnityEngine.Events;

    public class IntScriptableMultiEventListenerComponent : ScriptableMultiEventGenericListenerComponent<int, IntScriptableEvent, IntScriptableMultiEventListenerComponent.IntScriptableUnityEvent>
    {
        [Serializable]
        public class IntScriptableUnityEvent : UnityEvent<int>
        {
        }
    }
}
