﻿namespace StateObjects.Events.ListenerComponents
{
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class ScriptableEventGenericListenerComponent<T, TEvent, TUnityEvent> : MonoBehaviour, IScriptableEventListener<T> where TEvent : ScriptableEvent<T> where TUnityEvent : UnityEvent<T>
    {
        
        [SerializeField]
        private TEvent _event;

        [SerializeField]
        private TUnityEvent _eventGeneric;

        public void OnEnable()
        {
            Debug.Assert(_event != null, "ScriptableEventListener should always have a event assigend.", this);
            _event.RegisterListener(this);
        }

        public void OnDisable()
        {
            _event.UnregisterListener(this);
        }

        public void OnEventRaised(T @event)
        {
            if(_eventGeneric != null)
                _eventGeneric.Invoke(@event);
        }
    }
}
