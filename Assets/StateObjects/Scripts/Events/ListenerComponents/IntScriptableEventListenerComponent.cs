﻿namespace StateObjects.Events.ListenerComponents
{
    using System;
    using Events;
    using UnityEngine.Events;

    public class IntScriptableEventListenerComponent : ScriptableEventGenericListenerComponent<int, IntScriptableEvent, IntScriptableEventListenerComponent.IntScriptableUnityEvent>
    {
        [Serializable]
        public class IntScriptableUnityEvent : UnityEvent<int>
        {
        }
    }
}
