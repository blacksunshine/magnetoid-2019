﻿namespace StateObjects.Events.ListenerComponents
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class ScriptableMultiEventGenericListenerComponent<T, TEvent, TUnityEvent> : MonoBehaviour, IScriptableEventListener<T> where TEvent : ScriptableEvent<T> where TUnityEvent : UnityEvent<T>
    {

        [SerializeField]
        private List<TEvent> _events;

        [SerializeField]
        private TUnityEvent _eventGeneric;

        public void OnEnable()
        {
            Debug.Assert(_events != null && _events.Count > 0, "ScriptableEventListener should always have a event assigend.", this);
            foreach(var evt in _events)
            {
                evt.RegisterListener(this);
            }
        }

        public void OnDisable()
        {
            foreach (var evt in _events)
            {
                evt.UnregisterListener(this);
            }
        }

        public void OnEventRaised(T @event)
        {
            if (_eventGeneric != null)
                _eventGeneric.Invoke(@event);
        }
    }
}
