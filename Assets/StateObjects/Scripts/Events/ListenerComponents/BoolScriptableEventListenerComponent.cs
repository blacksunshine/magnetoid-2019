﻿namespace StateObjects.Events.ListenerComponents
{
    using System;
    using Events;
    using UnityEngine.Events;

    public class BoolScriptableEventListenerComponent : ScriptableEventGenericListenerComponent<bool, BoolScriptableEvent, BoolScriptableEventListenerComponent.BoolScriptableUnityEvent>
    {
        [Serializable]
        public class BoolScriptableUnityEvent : UnityEvent<bool>
        {
        }
    }
}
