﻿namespace StateObjects.Events.ListenerComponents
{
    using System.Collections.Generic;
    using Events;
    using UnityEngine;
    using UnityEngine.Events;

    public class ScriptableMultiEventListenerComponent : MonoBehaviour, IScriptableEventListener
    {
        [SerializeField]
        private List<ScriptableEvent> _events;

        [SerializeField]
        private UnityEvent _unityEvent;

        public void OnEnable()
        {
            Debug.Assert(_events != null && _events.Count > 0, "ScriptableEventListener should always have a event assigend.", this);
            foreach (var evt in _events)
            {
                evt.RegisterListener(this);
            }
            
        }

        public void OnDisable()
        {
            foreach (var evt in _events)
            {
                evt.UnregisterListener(this);
            }
        }

        public void OnEventRaised(string variableName)
        {
            if (_unityEvent != null)
            {
                _unityEvent.Invoke();
            }
        }
    }
}
