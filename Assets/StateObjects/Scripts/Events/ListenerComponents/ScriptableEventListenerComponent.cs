﻿namespace StateObjects.Events.ListenerComponents
{
    using Events;
    using UnityEngine;
    using UnityEngine.Events;

    public class ScriptableEventListenerComponent : MonoBehaviour, IScriptableEventListener
    {
        [SerializeField]
        private ScriptableEvent _event;

        [SerializeField]
        private UnityEvent _unityEvent;

        public void OnEnable()
        {
            Debug.Assert(_event != null, "ScriptableEventListener should always have a event assigend.", this);
            _event.RegisterListener(this);
        }

        public void OnDisable()
        {
            _event.UnregisterListener(this);
        }

        public void OnEventRaised(string variableName)
        {
            if(_unityEvent != null)
            {
                _unityEvent.Invoke();
            }
        }
    }
}
