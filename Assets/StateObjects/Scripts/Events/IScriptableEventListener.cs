﻿namespace StateObjects.Events
{
    public interface IScriptableEventListener
    {
        void OnEventRaised(string variableName);
    }
}