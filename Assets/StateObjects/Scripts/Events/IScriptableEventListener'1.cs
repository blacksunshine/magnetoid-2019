﻿namespace StateObjects.Events
{
    public interface IScriptableEventListener<in T>
    {
        void OnEventRaised(T @event);
    }
}