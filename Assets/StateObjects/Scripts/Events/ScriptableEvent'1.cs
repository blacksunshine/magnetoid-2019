﻿namespace StateObjects.Events
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public abstract class ScriptableEvent<T> : ScriptableObject
    {
        private readonly List<IScriptableEventListener<T>> _listeners = new List<IScriptableEventListener<T>>();

        public void Raise(T value)
        {
            for (var i = _listeners.Count - 1; i >= 0; i--)
                _listeners[i].OnEventRaised(value);
        }

#if UNITY_EDITOR
        [NonSerialized]
        public T EditorOnlyField;


        public void RaiseEditor()
        {
            Raise(EditorOnlyField);
        }
#endif
        public void RegisterListener(IScriptableEventListener<T> listener)
        {
            if (!_listeners.Contains(listener))
                _listeners.Add(listener);
        }

        public void UnregisterListener(IScriptableEventListener<T> listener)
        {
            if (_listeners.Contains(listener))
                _listeners.Remove(listener);
        }
    }
}
