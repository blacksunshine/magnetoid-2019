﻿using UnityEditor;
using UnityEngine;

namespace StateObjects.Events.Editor
{
    [CustomEditor(typeof(ScriptableEvent<>), true)]
    public class ScriptableEventGenericEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            GUI.enabled = Application.isPlaying;
            
            var prop = serializedObject.FindProperty("_editorOnlyField");

            if (prop == null)
            {
                EditorGUILayout.LabelField("Parameter can't be viewed in editor.");
                return;
            }

            EditorGUILayout.PropertyField(prop, new GUIContent("Parameter to send"));
            
            if (GUILayout.Button("Raise"))
                // ReSharper disable once PossibleNullReferenceException
                target.GetType().GetMethod("RaiseEditor").Invoke(target, null);
        }
    }
}
