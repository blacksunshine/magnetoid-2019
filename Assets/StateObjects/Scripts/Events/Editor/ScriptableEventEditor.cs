﻿using StateObjects.Events.Events;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Events.Editor
{
    [CustomEditor(typeof(ScriptableEvent))]
    public class ScriptableEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            var e = (ScriptableEvent)target;
            if (GUILayout.Button("Raise"))
                e.Raise();
        }
    }
}
