﻿using StateObjects.Events.Events;
using UnityEditor;
using UnityEngine;

namespace StateObjects.Events.Editor
{
    public static class CreateScriptableEventMenu
    {
        /// <summary>
        /// Create new asset from <see cref="ScriptableObject"/> type with unique name at
        /// selected folder in project window. Asset creation can be cancelled by pressing
        /// escape key when asset is initially being named.
        /// </summary>
        /// <typeparam name="T">Type of scriptable object.</typeparam>
        private static void CreateAsset<T>() where T : ScriptableObject
        {
            var asset = ScriptableObject.CreateInstance<T>();
            ProjectWindowUtil.CreateAsset(asset, "New " + typeof(T).Name + ".asset");
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableEvent/Event")]
        private static void CreateEvent()
        {
            CreateAsset<ScriptableEvent>();
        }

        [MenuItem("Assets/Create/Bright Future/ScriptableEvent/Int Event")]
        private static void CreateIntVariable()
        {
            CreateAsset<IntScriptableEvent>();
        }
    }
}
