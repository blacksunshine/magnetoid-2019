﻿namespace StateObjects.Events.Events
{
    using System.Collections.Generic;
    using UnityEngine;

    public class ScriptableEvent : ScriptableObject
    {
        private readonly List<IScriptableEventListener> _listeners = new List<IScriptableEventListener>();

        public void Raise()
        {
            for (var i = _listeners.Count - 1; i >= 0; i--)
                _listeners[i].OnEventRaised(name);
        }

        public void RegisterListener(IScriptableEventListener listener)
        {
            if (!_listeners.Contains(listener))
                _listeners.Add(listener);
        }

        public void UnregisterListener(IScriptableEventListener listener)
        {
            if (_listeners.Contains(listener))
                _listeners.Remove(listener);
        }
    }
}
