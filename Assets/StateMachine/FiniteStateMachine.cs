﻿namespace StateMachine
{
    public class FiniteStateMachine<T>
    {
        public T Context { get; private set; }
        public AFiniteStateMachineState<T> CurrentState { get; private set; }
        public AFiniteStateMachineState<T> PreviousState { get; private set; }

        private AFiniteStateMachineState<T> _nextState;

        public FiniteStateMachine(T context)
        {
            Context = context;
        }

        public FiniteStateMachine(T context, AFiniteStateMachineState<T> startState) : this(context)
        {
            ChangeState(startState);
        }

        public void Tick(float deltaTime)
        {
            if (_nextState != null)
            {
                if (CurrentState != null)
                    CurrentState.End();

                PreviousState = CurrentState;
                CurrentState = _nextState;
                _nextState = null;

                CurrentState.Begin(this, Context);
            }

            if (CurrentState != null)
            {
                CurrentState.Tick(deltaTime);
            }
        }

        public TState ChangeState<TState>() where TState : AFiniteStateMachineState<T>, new()
        {
            return ChangeState(new TState());
        }

        public TState ChangeState<TState>(TState state) where TState : AFiniteStateMachineState<T>
        {
            _nextState = state;
            return state;
        }
    }
}