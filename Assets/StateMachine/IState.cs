﻿namespace StateMachine
{
    public interface IState<in TMachine, in TContext>
    {
        void Begin(TMachine machine, TContext context);
        void Tick(float deltaTime);
        void End();
    }
}
