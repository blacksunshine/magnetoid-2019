﻿namespace StateMachine
{
    public class BaseState<TMachine, TContext> : IState<TMachine, TContext>
    {
        public TMachine Machine { get; set; }
        public TContext Context { get; set; }
        public float TimeInState { get; set; }

        public void Begin(TMachine machine, TContext context)
        {
            TimeInState = 0;
        }

        public void Tick(float deltaTime)
        {
            TimeInState += deltaTime;
        }

        public void End()
        {
        }
    }
}
