﻿namespace StateMachine
{
    public abstract class AFiniteStateMachineState<T> : IState<FiniteStateMachine<T>, T>
    {
        protected FiniteStateMachine<T> Machine { get; set; }
        protected T Context { get; set; }

        public virtual void Begin(FiniteStateMachine<T> machine, T context)
        {
            Machine = machine;
            Context = context;
        }

        public virtual void Tick(float deltaTime)
        {
            
        }

        public virtual void End()
        {
            
        }
    }
}