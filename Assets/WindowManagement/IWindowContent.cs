﻿namespace WindowManagement
{
    public interface IWindowContent
    {
        int Id { get; }
        bool IsOpen { get; }
    }
}
