﻿#if UNITY_EDITOR
#endif

namespace WindowManagement
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.SceneManagement;

    public struct EmptyValue
    {
        
    }

    [ExecuteInEditMode]
    public abstract class WindowContent : MonoBehaviour, IWindowContent
    {
        #region public state
        public event Action OnOpen;
        public event Action OnClose;

        public int Id => gameObject.scene.buildIndex;
        public bool IsOpen { get; private set; }

        #endregion

        private GameObject[] _rootTransforms;
        private AWindowHandle _handle;

#pragma warning disable 649
        [SerializeField]
        private PlayableDirector _openDirector;
        [SerializeField]
        private PlayableDirector _closeDirector;
#pragma warning restore 649

        protected virtual void Awake()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
#endif
                _rootTransforms = gameObject.scene.GetRootGameObjects();

                _handle = WindowManager.Instance.SetContent(this);
#if UNITY_EDITOR
            }
#endif

#if UNITY_EDITOR
            if (Application.isPlaying) return;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving += EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved += EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened += EditorSceneManager_sceneOpened;
#endif
        }

#if UNITY_EDITOR
        private void EditorSceneManager_sceneSaved(Scene scene)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneOpened(Scene scene, UnityEditor.SceneManagement.OpenSceneMode mode)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneSaving(Scene scene, string path)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(false);
            }
        }

        protected virtual void OnDestroy()
        {
            if (Application.isPlaying) return;

            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving -= EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved -= EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened -= EditorSceneManager_sceneOpened;
        }
#endif

        public virtual void Open()
        {
            IsOpen = true;
            if (OnOpen != null)
                OnOpen.Invoke();

            if (_openDirector != null)
            {
                _openDirector.Play();
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(true);
            }
        }

        public virtual void Close()
        {
            IsOpen = false;
            if (OnClose != null)
                OnClose.Invoke();

            if (_closeDirector != null)
            {
                _closeDirector.Play();

                StartCoroutine(Unload());
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(false);
            }

            WindowManager.Instance.CloseWindow(_handle);
        }

        private IEnumerator Unload()
        {
            yield return new WaitWhile(() => _closeDirector.state == PlayState.Playing);

            WindowManager.Instance.CloseWindow(_handle);
        }
    }

    [ExecuteInEditMode]
    public abstract class WindowContent<TOpen, TOut> : MonoBehaviour, IWindowContent
    {
        #region public state
        public event Action<TOpen> OnOpen;
        public event Action<TOut> OnClose;

        public int Id => gameObject.scene.buildIndex;
        public bool IsOpen { get; private set; }

        #endregion

        private GameObject[] _rootTransforms;
        private AWindowHandle _handle;

#pragma warning disable 649
        [SerializeField]
        private PlayableDirector _openDirector;
        [SerializeField]
        private PlayableDirector _closeDirector;
#pragma warning restore 649

        protected virtual void Awake()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
#endif
                _rootTransforms = gameObject.scene.GetRootGameObjects();


                _handle = WindowManager.Instance.SetContent(this);
#if UNITY_EDITOR
            }
#endif

#if UNITY_EDITOR
            if (Application.isPlaying) return;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving += EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved += EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened += EditorSceneManager_sceneOpened;
            UnityEditor.EditorApplication.playModeStateChanged += EditorApplicationOnPlayModeStateChanged;
#endif
        }
#if UNITY_EDITOR
        private void EditorApplicationOnPlayModeStateChanged( UnityEditor.PlayModeStateChange playModeStateChange)
        {
            switch (playModeStateChange)
            {
                case  UnityEditor.PlayModeStateChange.EnteredEditMode:
                    EditorSceneManager_sceneOpened(gameObject.scene, UnityEditor.SceneManagement.OpenSceneMode.Additive);
                    break;
                case  UnityEditor.PlayModeStateChange.ExitingEditMode:
                    EditorSceneManager_sceneSaving(gameObject.scene, "");
                    break;
                case  UnityEditor.PlayModeStateChange.EnteredPlayMode:
                    break;
                case  UnityEditor.PlayModeStateChange.ExitingPlayMode:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playModeStateChange", playModeStateChange, null);
            }
        }
#endif
#if UNITY_EDITOR
        private void EditorSceneManager_sceneSaved(Scene scene)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneOpened(Scene scene, UnityEditor.SceneManagement.OpenSceneMode mode)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneSaving(Scene scene, string path)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(false);
            }
        }

        protected virtual void OnDestroy()
        {
            if (Application.isPlaying) return;

            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving -= EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved -= EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened -= EditorSceneManager_sceneOpened;
        }
#endif

        public virtual void Open(TOpen infoObject)
        {
            IsOpen = true;
            if (OnOpen != null)
                OnOpen.Invoke(infoObject);

            if (_openDirector != null)
            {
                _openDirector.Play();
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(true);
            }
        }

        public virtual void Close(TOut infoObject)
        {
            IsOpen = false;
            if (OnClose != null)
                OnClose.Invoke(infoObject);

            if (_closeDirector != null)
            {
                _closeDirector.Play();

                StartCoroutine(Unload());
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(false);
            }

            WindowManager.Instance.CloseWindow(_handle);
        }

        private IEnumerator Unload()
        {
            yield return new WaitWhile(() => _closeDirector.state == PlayState.Playing);

            WindowManager.Instance.CloseWindow(_handle);
        }
    }

    [ExecuteInEditMode]
    public abstract class WindowContentOpen<TOpen> : MonoBehaviour, IWindowContent
    {
        #region public state
        public event Action<TOpen> OnOpen;
        public event Action OnClose;

        public int Id => gameObject.scene.buildIndex;
        public bool IsOpen { get; private set; }

        #endregion

        private GameObject[] _rootTransforms;
        private AWindowHandle _handle;

#pragma warning disable 649
        [SerializeField]
        private PlayableDirector _openDirector;
        [SerializeField]
        private PlayableDirector _closeDirector;
#pragma warning restore 649

        protected virtual void Awake()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
#endif
                _rootTransforms = gameObject.scene.GetRootGameObjects();


                _handle = WindowManager.Instance.SetContent(this);
#if UNITY_EDITOR
            }
#endif

#if UNITY_EDITOR
            if (Application.isPlaying) return;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving += EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved += EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened += EditorSceneManager_sceneOpened;
            UnityEditor.EditorApplication.playModeStateChanged += EditorApplicationOnPlayModeStateChanged;
#endif
        }
#if UNITY_EDITOR
        private void EditorApplicationOnPlayModeStateChanged(UnityEditor.PlayModeStateChange playModeStateChange)
        {
            switch (playModeStateChange)
            {
                case UnityEditor.PlayModeStateChange.EnteredEditMode:
                    EditorSceneManager_sceneOpened(gameObject.scene, UnityEditor.SceneManagement.OpenSceneMode.Additive);
                    break;
                case UnityEditor.PlayModeStateChange.ExitingEditMode:
                    EditorSceneManager_sceneSaving(gameObject.scene, "");
                    break;
                case UnityEditor.PlayModeStateChange.EnteredPlayMode:
                    break;
                case UnityEditor.PlayModeStateChange.ExitingPlayMode:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("playModeStateChange", playModeStateChange, null);
            }
        }
#endif
#if UNITY_EDITOR
        private void EditorSceneManager_sceneSaved(Scene scene)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneOpened(Scene scene, UnityEditor.SceneManagement.OpenSceneMode mode)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(true);
            }
        }

        private void EditorSceneManager_sceneSaving(Scene scene, string path)
        {
            if (scene != gameObject.scene) return;

            var rootGameObjects = scene.GetRootGameObjects();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < rootGameObjects.Length; ++i)
            {
                if (rootGameObjects[i].gameObject == gameObject) continue;

                rootGameObjects[i].gameObject.SetActive(false);
            }
        }

        protected virtual void OnDestroy()
        {
            if (Application.isPlaying) return;

            UnityEditor.SceneManagement.EditorSceneManager.sceneSaving -= EditorSceneManager_sceneSaving;
            UnityEditor.SceneManagement.EditorSceneManager.sceneSaved -= EditorSceneManager_sceneSaved;
            UnityEditor.SceneManagement.EditorSceneManager.sceneOpened -= EditorSceneManager_sceneOpened;
        }
#endif

        public virtual void Open(TOpen infoObject)
        {
            IsOpen = true;
            if (OnOpen != null)
                OnOpen.Invoke(infoObject);

            if (_openDirector != null)
            {
                _openDirector.Play();
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(true);
            }
        }

        public virtual void Close()
        {
            IsOpen = false;
            if (OnClose != null)
                OnClose.Invoke();

            if (_closeDirector != null)
            {
                _closeDirector.Play();

                StartCoroutine(Unload());
                return;
            }

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < _rootTransforms.Length; ++i)
            {
                _rootTransforms[i].gameObject.SetActive(false);
            }

            WindowManager.Instance.CloseWindow(_handle);
        }

        private IEnumerator Unload()
        {
            yield return new WaitWhile(() => _closeDirector.state == PlayState.Playing);

            WindowManager.Instance.CloseWindow(_handle);
        }
    }
}
