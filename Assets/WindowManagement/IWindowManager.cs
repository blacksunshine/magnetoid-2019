﻿namespace WindowManagement
{
    public interface IWindowManager
    {
        WindowHandle<T> OpenWindow<T>(int id) where T : IWindowContent;
        void CloseWindow(AWindowHandle windowHandle);
        AWindowHandle SetContent<T>(T windowContent) where T : IWindowContent;
    }
}
