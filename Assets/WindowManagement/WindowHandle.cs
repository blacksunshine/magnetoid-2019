﻿namespace WindowManagement
{
    using System.Diagnostics;

    public class WindowHandle<T> : AWindowHandle where T : IWindowContent
    {
        public T Content { get; private set; }

        public WindowHandle(int id, IWindowManager manager) : base(id, manager)
        {
        }

        public override void SetContent(IWindowContent content)
        {
            Debug.Assert(content is T, "Content is not of type " + typeof(T), "Much detail. Such wow.");
            Content = (T) content;
            IsLoaded = true;
        }
    }
}
