﻿namespace WindowManagement
{
    public abstract class AWindowHandle
    {
        public readonly int Id;
        public bool IsLoaded { get; protected set; }
        public IWindowManager WindowManager { get; private set; }

        protected AWindowHandle(int id, IWindowManager manager)
        {
            Id = id;
            WindowManager = manager;
        }

        public abstract void SetContent(IWindowContent content);
    }
}
