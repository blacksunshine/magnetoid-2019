﻿namespace WindowManagement
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class WindowManager : MonoBehaviour, IWindowManager
    {
        public static IWindowManager Instance { get; private set; }

        private readonly Dictionary<int, AWindowHandle> _openWindowInfo = new Dictionary<int, AWindowHandle>();

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.Log("Window Manager is already set.", this);
                Destroy(this);
            }
        }

        public WindowHandle<T> OpenWindow<T>(int id) where T : IWindowContent
        {
            AWindowHandle handle;

            if (_openWindowInfo.TryGetValue(id, out handle))
            {
                return handle as WindowHandle<T>;
            }

            handle = new WindowHandle<T>(id, this);
            _openWindowInfo.Add(handle.Id, handle);
            SceneManager.LoadSceneAsync(handle.Id, LoadSceneMode.Additive);
            return (WindowHandle<T>)handle;
        }

        public void CloseWindow(AWindowHandle windowHandle)
        {
            _openWindowInfo.Remove(windowHandle.Id);
            SceneManager.UnloadSceneAsync(windowHandle.Id);
        }

        public AWindowHandle SetContent<T>(T windowContent) where T : IWindowContent
        {
            AWindowHandle handle;
            if (!_openWindowInfo.TryGetValue(windowContent.Id, out handle))
            {
                throw new InvalidOperationException("The scene " + windowContent.Id + " has no handle.");
            }

            handle.SetContent(windowContent);
            return handle;
        }
    }
}
