﻿namespace Magnetoid
{
    using StateMachine;
    using WindowManagement;
    using UnityEngine;
    using Magnetoid.AppState;

    public class AppManager : MonoBehaviour
    {
        [SerializeField]
        private WindowManager _windowManager;

        private FiniteStateMachine<AppManager> _stateMachine;

        private void Awake()
        {
            Cursor.visible = false;

            _stateMachine = new FiniteStateMachine<AppManager>(this, new SplashScreenState());

            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            _stateMachine.Tick(Time.deltaTime);
        }

        public IWindowManager GetWindowManager()
        {
            return _windowManager;
        }

    }
}
