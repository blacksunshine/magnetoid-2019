﻿namespace Magnetoid
{
    using UnityEngine;

    public static class ExtensionMethods
    {
        public static void DestroyChildren(this Transform parent)
        {
            foreach (Transform child in parent)
            {
                Object.Destroy(child.gameObject);
            }
        }

        public static void DestroyChildrenImmediate(this Transform parent)
        {
            while (parent.childCount > 0)
            {
                Object.DestroyImmediate(parent.GetChild(0).gameObject);
            }
        }

        public static void SetLayerRecursively(this GameObject obj, int layer)
        {
            obj.layer = layer;

            foreach (Transform child in obj.transform)
            {
                child.gameObject.SetLayerRecursively(layer);
            }
        }
    }
}

