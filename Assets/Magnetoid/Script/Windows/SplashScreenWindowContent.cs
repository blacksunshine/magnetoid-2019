﻿namespace Magnetoid.Windows
{
    using WindowManagement;
    using System;

    public class SplashScreenWindowContent : WindowContent
    {
        public Action OnPlaySelected;
        public Action OnExitSelected;

        public void OnPlayButtonPressed()
        {
            OnPlaySelected?.Invoke();
        }

        public void OnExitButtonPressed()
        {
            OnExitSelected?.Invoke();
        }
    }
}

