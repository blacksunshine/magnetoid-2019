﻿namespace Magnetoid.Windows
{
    using WindowManagement;
    using System;

    public class HUDWindowContent : WindowContent
    {
        public Action OnExitSelected;

        public void OnExitPressed()
        {
            OnExitSelected?.Invoke();
        }
    }
}

