﻿namespace Magnetoid.Windows
{
    using System;
    using WindowManagement;

    public class MainMenuWindowContent : WindowContent
    {
        public Action OnGameStartSelected;
        public Action OnExitSelected;

        public void OnExitButtonPressed()
        {
            OnExitSelected?.Invoke();
        }

        public void OnGameStartPressed()
        {
            OnGameStartSelected?.Invoke();
        }
    }
}
    
