﻿namespace Magnetoid.AppState
{
    using System;
    using Magnetoid.Windows;
    using StateMachine;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using WindowManagement;

    public class GameState : AFiniteStateMachineState<AppManager>
    {
        private WindowHandle<HUDWindowContent> _handle;
        private AsyncOperation _gameSceneLoadingAsyncOperation;

        public override void Begin(FiniteStateMachine<AppManager> machine, AppManager context)
        {
            base.Begin(machine, context);
            _gameSceneLoadingAsyncOperation = SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
            _handle = context.GetWindowManager().OpenWindow<HUDWindowContent>(2);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            if (_handle.IsLoaded && !_handle.Content.IsOpen)
            {
                _handle.Content.Open();
                _handle.Content.OnExitSelected += OnExitSelected;
            }

            if (_gameSceneLoadingAsyncOperation != null && _gameSceneLoadingAsyncOperation.isDone)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game"));
                _gameSceneLoadingAsyncOperation = null;
            }
        }

        private void OnExitSelected()
        {
            SceneManager.UnloadSceneAsync("Game");
            _handle.Content.Close();
            Machine.ChangeState(new MainMenuState());
        }
    }
}

