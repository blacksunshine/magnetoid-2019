﻿namespace Magnetoid.AppState
{
    using StateMachine;
    using WindowManagement;
    using Windows;
    using UnityEngine.SceneManagement;
    using UnityEngine;

    public class SplashScreenState : AFiniteStateMachineState<AppManager>
    {
        private WindowHandle<SplashScreenWindowContent> _handle;

        public override void Begin(FiniteStateMachine<AppManager> machine, AppManager context)
        {
            base.Begin(machine, context);
            _handle = context.GetWindowManager().OpenWindow<SplashScreenWindowContent>(1);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            if (_handle.IsLoaded && !_handle.Content.IsOpen)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
                _handle.Content.Open();
                _handle.Content.OnPlaySelected += OnPlaySelected;
                _handle.Content.OnExitSelected += OnExitSelected;
            }
        }

        private void OnPlaySelected()
        {
            _handle.Content.Close();
            Machine.ChangeState(new MainMenuState());
        }

        private void OnExitSelected()
        {
            Application.Quit();
        }
    }
}
