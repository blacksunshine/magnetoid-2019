﻿namespace Magnetoid.AppState
{
    using System;
    using Magnetoid.Windows;
    using StateMachine;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using WindowManagement;

    public class MainMenuState : AFiniteStateMachineState<AppManager>
    {
        private WindowHandle<MainMenuWindowContent> _handle;

        public override void Begin(FiniteStateMachine<AppManager> machine, AppManager context)
        {
            base.Begin(machine, context);
            _handle = context.GetWindowManager().OpenWindow<MainMenuWindowContent>(2);
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);

            if (_handle.IsLoaded && !_handle.Content.IsOpen)
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));
                _handle.Content.Open();
                _handle.Content.OnGameStartSelected += OnGameStartSelected;
                _handle.Content.OnExitSelected += OnExitSelected;
            }
        }

        private void OnExitSelected()
        {
            Application.Quit();
        }

        private void OnGameStartSelected()
        {
            _handle.Content.Close();
            Machine.ChangeState(new GameState());
        }
    }
}
    
